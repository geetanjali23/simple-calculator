let clickedButtonValue = "0";
let variableTotalValue = 0;
let previousOperator;
const button = document.querySelectorAll("button");
const display = document.querySelector(".displayScreen");

function initialCall(event) {
  buttonClicked(event.target.innerText);
}

for (let i = 0; i < button.length; i++) {
  button[i].addEventListener("click", initialCall);
}

function buttonClicked(value) {
  if (isNaN(parseInt(value))) {
    handleSymbols(value);
  } else {
    handleNumber(value);
  }
  rerender();
}
function handleNumber(value) {
  if (clickedButtonValue === "0") {
    clickedButtonValue = value;
  } else {
    clickedButtonValue += value;
  }
}
function rerender() {
  display.innerText = clickedButtonValue;
}

function handleSymbols(value) {
  if (value === "C") {
    clickedButtonValue = "0";
    variableTotalValue = 0;
    return;
  } else if (value === "=") {
    if (previousOperator === null) {
      return;
    }
    calculateSum(parseInt(clickedButtonValue));
    previousOperator = null;
    clickedButtonValue = +variableTotalValue;
    variableTotalValue = 0;
    return;
  } else if (value === "D") {
    if (clickedButtonValue.length === 1) {
      clickedButtonValue = "0";
      return;
    } else {
      clickedButtonValue = clickedButtonValue.substring(
        0,
        clickedButtonValue.length - 1
      );
      return;
    }
  } else if (value === "+" || value === "-" || value === "/" || value === "x") {
    console.log(value);

    handleMathCalculation(value);
    return;
  }
}

function handleMathCalculation(value) {
  if (clickedButtonValue === "0") {
    return;
  }
  const intValue = parseInt(clickedButtonValue);
  if (variableTotalValue === 0) {
    variableTotalValue = intValue;
  } else {
    calculateSum(intValue);
  }

  previousOperator = value;
  clickedButtonValue = "0";
  console.log(variableTotalValue);
}
function calculateSum(clickedValue) {
  if (previousOperator === "+") {
    variableTotalValue += clickedValue;
  } else if (previousOperator === "-") {
    variableTotalValue -= clickedValue;
  } else if (previousOperator === "x") {
    variableTotalValue *= clickedValue;
  } else {
    variableTotalValue /= clickedValue;
  }
}
